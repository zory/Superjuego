import os

template="""<RCC>
    <qresource prefix="/">
{files}    </qresource>
</RCC>
"""

directories = ["stories","mails","templates","sounds"]
files = ""
for d in directories:
    for f in os.listdir(d):
        if not f.startswith('.'):
            files += "        <file>" + d + "/" + f + "</file>\n"

output = template.format(files=files)
print(output)

