// Presentación del trabajo y primer reply
Sleep;3
LoadMail;direccion@justicia.aragon.es|presentacion
LoadTemplate;presentacion_ok
WaitReply;direccion@justicia.aragon.es|presentacion_ok
DelTemplate;presentacion_ok

// Enviar un correo a rrhh identificandose
Sleep;5
LoadMail;direccion@justicia.aragon.es|instrucciones_enviar
LoadTemplate;alta_rrhh
LoadContact;rrhh@justicia.aragon.es
WaitReply;rrhh@justicia.aragon.es|alta_rrhh
DelTemplate;alta_rrhh

// Cargar primera tarea
Sleep;5
LoadMail;rrhh@justicia.aragon.es|alta_ok
LoadStory;reclamacion0
