#ifndef TEMPLATE_H
#define TEMPLATE_H

#include <QString>



class Template
{
public:
    Template();
    Template(const QString name, const QString subject, const QString text);

    QString name;
    QString subject;
    QString text;
    static Template *loadTemplate(QString filename);
};

#endif // TEMPLATE_H
