#include "mainwindow.h"
#include "replymailwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QFile>
#include <QSortFilterProxyModel>
#include <QStringListModel>
#include <QThread>
#include <QTimer>
#include <time.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    for (int tray = 0; tray < TRAY_SIZE; tray++) {
        this->listTrayModel[tray] = new MailModel();
    }
    this->mailModel = this->listTrayModel[INBOX_TRAY];

    this->contactModel = new QStringList();
    this->contactModel->append("");
    this->templateModel = new TemplateModel();

    ui->mailList->setModel(mailModel->filter);
    ui->mailList->setSortingEnabled(true);
//    ui->mailList->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);

    sounds[SOUND_NEW_MAIL] = new QMediaPlayer(this);
    sounds[SOUND_NEW_MAIL]->setMedia(QUrl("qrc:/sounds/new_mail.wav"));
    sounds[SOUND_CORRECT_MAIL] = new QMediaPlayer(this);
    sounds[SOUND_CORRECT_MAIL]->setMedia(QUrl("qrc:/sounds/correct_mail.mp3"));
    sounds[SOUND_INCORRECT_MAIL] = new QMediaPlayer(this);
    sounds[SOUND_INCORRECT_MAIL]->setMedia(QUrl("qrc:/sounds/incorrect_mail.mp3"));
    for (QMediaPlayer *s : sounds) {
        s->setVolume(5);
    }

    QStringListModel *trays = new QStringListModel();
    trays->setStringList({"Entrada","Salida"});
    ui->trayList->setModel(trays);

    // Descomentar para borrar la partida guardada
    state.clear();

    score = 0;
    if (loadGame()) {
        // First time, no savefile
        current_stories.append(Story::loadStory("bienvenida"));
//        current_stories.append(Story::loadStory("amigo0"));
        continueStory(0);
    } else {
        for(int i = 0; i < current_stories.length(); i++) {
            Story *s = current_stories.at(i);
            // Calcular cuando seguir la historia
            s->until_valid = false;
            time_t now = std::time(0) * 1000;
            time_t wait_time = s->until - now;
            if (wait_time > 0) {
                QTimer::singleShot(wait_time,this,[=](){continueStory(i);});
            } else {
                s->until_valid = true;
                QTimer::singleShot(100,this,[=](){continueStory(i);});
            }
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_mailList_clicked(const QModelIndex &index)
{
    int row = mailModel->filter->mapToSource(index).row();
    int tray = ui->trayList->currentIndex().row();
    mailModel->mDatas[row]->unread = false;
    QString html = mailModel->getMailHtml(row,tray);
    ui->mailView->setHtml(html);
}

void MainWindow::on_compose_clicked()
{
    ReplyMailWindow *w = new ReplyMailWindow(this);
    w->setModal(true);
    w->exec();
}

void MainWindow::on_reply_clicked()
{
    QModelIndexList indexes = ui->mailList->selectionModel()->selectedIndexes();
    if (!indexes.isEmpty()) {
        ReplyMailWindow *w = new ReplyMailWindow(this,mailModel->getMailContact(indexes.first()));
        w->setModal(true);
        w->exec();
    } else {
        qDebug() << "TODO: implementar cuando no hay correo seleccionado";
    }
}

void MainWindow::continueStory(int i) {
    assert(i < current_stories.length());
    Story *current_story = this->current_stories.at(i);
    if(current_story == nullptr) {
        qDebug() << "End story";
        return;
    }

    qDebug() << "Executing:" << current_story->get_type() << current_story->list2load->join(";") << "Story:" << current_story->name;

    if(current_story->node_type == Story::LoadMail) {
        sounds[SOUND_NEW_MAIL]->play();
        for(const QString &mail : *current_story->list2load) {
            QStringList split_mail = mail.split(SEPARATOR2);
            assert(split_mail.length() == 2);
            long date = current_story->until_valid ? current_story->until/1000 : 0;
            this->listTrayModel[INBOX_TRAY]->addMail(Mail::loadMail(split_mail[1],split_mail[0],true,date));
        }
        current_stories.replace(i,current_story->next_node);
        continueStory(i);
    } else if(current_story->node_type == Story::LoadCommonMail) {
        sounds[SOUND_NEW_MAIL]->play();
        QStringList *list = current_story->list2load;

        // Primer string de contacto|archivo
        QString mail_string = list->at(0);
        QString gaps_string = list->at(1);
        QStringList split_mail = mail_string.split(SEPARATOR2);
        QStringList split_gaps = gaps_string.split(SEPARATOR2);
        assert(split_mail.length() == 2);
        long date = current_story->until_valid ? current_story->until/1000 : 0;
        Mail *mail = Mail::loadMail(split_mail[1],split_mail[0],true,date);

        for (int i = 0; i < split_gaps.length(); i++) {
            QString text2replace = "<"+QString::number(i)+">";
            mail->text = mail->text.replace(text2replace,split_gaps.at(i));
        }

        this->listTrayModel[INBOX_TRAY]->addMail(mail);
        current_stories.replace(i,current_story->next_node);
        continueStory(i);
    } else if(current_story->node_type == Story::LoadContact) {
        for(const QString &contact : *current_story->list2load) {
            contactModel->append(contact);
        }
        current_stories.replace(i,current_story->next_node);
        continueStory(i);
    } else if(current_story->node_type == Story::LoadTemplate) {
        for(const QString &tem : *current_story->list2load) {
            templateModel->addTemplate(Template::loadTemplate(tem));
        }
        current_stories.replace(i,current_story->next_node);
        continueStory(i);
    } else if(current_story->node_type == Story::DelContact) {
        for(const QString &contact : *current_story->list2load) {
            contactModel->removeAll(contact);
        }
        current_stories.replace(i,current_story->next_node);
        continueStory(i);
    } else if(current_story->node_type == Story::DelTemplate) {
        for(const QString &tem : *current_story->list2load) {
            templateModel->removeTemplate(tem);
        }
        current_stories.replace(i,current_story->next_node);
        continueStory(i);
    } else if(current_story->node_type == Story::LoadStory) {
        Story *new_story = Story::loadStory(current_story->list2load->first());
        current_stories.append(new_story);
        continueStory(current_stories.length()-1); // Ejecutar nueva historia primero
        current_stories.replace(i,current_story->next_node);
        continueStory(i);
    } else if(current_story->node_type == Story::Sleep) {
        time_t time = current_story->list2load->first().toInt() * 1000;
#if DEBUG_SLEEP
        time = 5;
#endif
        current_stories.replace(i,current_story->next_node);
        time_t now = std::time(0) * 1000;
        current_story->until += time;
        if (current_story->until_valid && current_story->until <= now) { // El sleep se tendria que haber ejecutado
            continueStory(i);
        } else if (current_story->until_valid) { // Parte del sleep se tendria que haber ejecutado
            current_story->until_valid = false;
            time = current_story->until - now;
            QTimer::singleShot(time,this,[=](){continueStory(i);});
        } else { // Ejecutar sleep entero
            current_story->until = now + time;
            QTimer::singleShot(time,this,[=](){continueStory(i);});
        }
    } else if(current_story->node_type == Story::WaitReply || current_story->node_type == Story::WaitReplyCond) {
        current_story->until_valid = false;
    } else if (current_story->node_type == Story::GoTo) {
        current_stories.replace(i,current_story->goto_node);
        continueStory(i);
    }
}

void MainWindow::addMailExitTray(QString recipient, Template *t, QStringList *answers, time_t timestamp) {
    QString template_name = t->name;
    QString text = t->text;
    for (const QString &a : *answers) {
        int start = text.indexOf("{");
        int end = text.indexOf("}");
        text.replace(start,end-start+1,a);
    }
    // Añadir mail a la bandeja de salida
    Mail *exitMail = new Mail(template_name,recipient,t->subject,text,0,timestamp);
    exitMail->answers = answers;
    this->listTrayModel[EXIT_TRAY]->addMail(exitMail);
}

void MainWindow::recieveReply(QString recipient, Template *t, QStringList *answers)
{
    QString template_name = t->name;
    for(int i = 0; i < current_stories.length(); i++) {
        Story *current_story = this->current_stories.at(i);
        if (current_story == nullptr) continue;
        if(current_story->node_type == Story::WaitReply) {
            for (Reply* reply : current_story->replies) {
                if(reply->recipient != recipient || reply->template_name !=template_name) {
                    continue;
                } else {
                    addMailExitTray(recipient,t,answers);
                    sounds[SOUND_CORRECT_MAIL]->play();
                    qDebug() << "Reply match";
                    score++;
                    reply->answered = true;
                    for (Reply* reply_aux : current_story->replies) { // Check all replies
                        if (!reply_aux->answered) {
                            qDebug() << "Some reply is missing";
                            return;
                        }
                    }
                    qDebug() << "All replies match, continuing story...";
                    current_stories.replace(i,current_story->next_node);
                    continueStory(i);
                    return;
                }
            }
            sounds[SOUND_INCORRECT_MAIL]->play();
            qDebug() << "Reply incorrecto";
            return;
        } else if(current_story->node_type == Story::WaitReplyCond) {
            qDebug() << "Recieving reply, recipient:" << recipient << ", template:" << template_name;
            for (Reply* reply : current_story->replies) {
                qDebug() << "Trying reply, recipient:" << reply->recipient << ", template:" << reply->template_name;
                if(reply->recipient != recipient || reply->template_name !=template_name) {
                    continue;
                } else {
                    addMailExitTray(recipient,t,answers);
                    sounds[SOUND_CORRECT_MAIL]->play();
                    qDebug() << "Reply match, jumping...";
                    score++;
                    current_stories.replace(i,reply->next_node);
                    continueStory(i);
                    return;
                }
            }
            sounds[SOUND_INCORRECT_MAIL]->play();
            qDebug() << "Reply incorrecto";
            return;
        } else {
            qDebug() << "Historia incorrecta";
        }
    }
}

void MainWindow::saveGame() {
    qDebug() << "Guardando partida...";

    QStringList aux;

    for (Mail *m : this->listTrayModel[INBOX_TRAY]->mDatas) {
        aux.append(m->from + SEPARATOR2 + m->name + SEPARATOR2 + (m->unread ? "true" : "false") + SEPARATOR2 + QString::number(m->timestamp));
    }
    this->state.setValue("inboxTrayModel",aux.join(SEPARATOR1));

    aux.clear();
    for (Mail *m : this->listTrayModel[EXIT_TRAY]->mDatas) {
        aux.append(m->from + SEPARATOR2 + m->name + SEPARATOR2 + QString::number(m->timestamp) + SEPARATOR2 + m->answers->join(SEPARATOR2));
    }
    this->state.setValue("exitTrayModel",aux.join(SEPARATOR1));

    aux.clear();
    for (Template *t : templateModel->mDatas) {
        aux.append(t->name);
    }
    this->state.setValue("templateModel",aux.join(SEPARATOR1));

    this->state.setValue("contactModel",contactModel->join(SEPARATOR1));

    aux.clear();
    for (Story *s : current_stories) {
        if (s != nullptr)
            aux.append(s->name+SEPARATOR2+s->length()+SEPARATOR2+s->until);
    }
    this->state.setValue("current_stories",aux.join(SEPARATOR1));

    qDebug() << "Partida guardada correctamente";
}

// Return true if error
bool MainWindow::loadGame() {
    qDebug() << "Cargando partida...";
    if (this->state.value("current_story_name","-NoGame-").toString().compare("-NoGame-") == 0) {
        qDebug() << "No hay ninguna partida guardada";
        return true;
    }
    QStringList line = this->state.value("inboxTrayModel").toString().split(SEPARATOR1,Qt::SkipEmptyParts);
    for (QString mail : line) {
        QStringList mail_aux = mail.split(SEPARATOR2);
        bool unread = mail_aux[2].compare("true") == 0;
        this->listTrayModel[INBOX_TRAY]->addMail(Mail::loadMail(mail_aux[1],mail_aux[0],unread,mail_aux[3].toLong()));
    }
    line = this->state.value("exitTrayModel").toString().split(SEPARATOR1,Qt::SkipEmptyParts);
    for (QString mail : line) {
        QStringList mail_aux = mail.split(SEPARATOR2);
        QString recipient = mail_aux[0];
        Template *t = Template::loadTemplate(mail_aux[1]);
        time_t timestamp = mail_aux[2].toLong();
        QStringList *answers = new QStringList(mail_aux.mid(3));

        addMailExitTray(recipient,t,answers,timestamp);
    }
    line = this->state.value("templateModel").toString().split(SEPARATOR1,Qt::SkipEmptyParts);
    for (QString temp : line) {
        templateModel->addTemplate(Template::loadTemplate(temp));
    }
    line = this->state.value("contactModel").toString().split(SEPARATOR1,Qt::SkipEmptyParts);
    for (QString contact : line) {
        contactModel->append(contact);
    }

    QStringList stories = this->state.value("current_stories").toString().split(SEPARATOR1,Qt::SkipEmptyParts);
    for(QString s : stories) {
        QStringList list = s.split(SEPARATOR2,Qt::SkipEmptyParts);
        QString name = list[0];
        int length = list[1].toInt();
        time_t until = list[2].toLongLong();
        Story *story_node = Story::loadStory(name);
        int remaining_nodes = story_node->length() - length;
        assert(remaining_nodes >= 0 && length >= 0);
        while (remaining_nodes > 0) {
            story_node = story_node->next_node;
            remaining_nodes--;
        }
        story_node->until = until;
        current_stories.append(story_node);
    }

    qDebug() << "Partida cargada correctamente";

    return false;
}

void MainWindow::closeEvent(QCloseEvent *event) {
    Q_UNUSED(event);
    saveGame();
}

void MainWindow::on_trayList_clicked(const QModelIndex &index)
{
    mailModel = this->listTrayModel[index.row()];
    ui->mailList->setModel(mailModel->filter);
}
