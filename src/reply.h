#ifndef REPLY_H
#define REPLY_H

#include <QString>
//#include "story.h"

class Story;

class Reply
{
public:
    Reply();
    ~Reply();

    QString recipient;
    QString template_name;
    QStringList *answers;
    Story *next_node;
    bool answered;
};

#endif // REPLY_H
