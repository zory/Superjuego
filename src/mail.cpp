#include "mail.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>

// Ejemplo
Mail::Mail()
{
}

Mail::Mail(const QString &filename, const QString &from, const QString &subject, const QString &text, bool unread, time_t timestamp)
{
    this->name = filename;
    this->from = from;
    this->subject = subject;
    this->text = text;
    this->unread = unread;
    if (timestamp == 0)
        this->timestamp = std::time(0);
    else
        this->timestamp = timestamp;
    this->answers = nullptr;
}

Mail* Mail::loadMail(QString filename, QString from, bool unread, time_t timestamp)
{
    QString filename_mail = ":/mails/" + filename + ".mail";
    QFile file(filename_mail);
    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "No se puede cargar el mail" << filename_mail;
        return nullptr;
    }
    QTextStream *stream = new QTextStream(&file);
    stream->setCodec("UTF-8");
    QString mailSubject = stream->readLine();
    QString mailText = stream->readAll();
    Mail *mail = new Mail(filename,from,mailSubject,mailText,unread,timestamp);
    file.close();
    return mail;
}
