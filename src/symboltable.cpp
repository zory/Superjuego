#include "symboltable.h"
#include <QDebug>

SymbolTable::SymbolTable()
{

}

void SymbolTable::addTag(QString tag, Story *story)
{
    if (tags.count(tag) == 0) {
        tags.insert(tag,story);
    } else {
        qDebug() << "Multiple definitions of " << tag;
    }
}

void SymbolTable::addGoto(QString tag, Story **reply)
{
    if (gotos.count(tag) == 0) {
        QList<Story**> *list = new QList<Story**>();
        list->append(reply);
        gotos.insert(tag,list);
    } else {
        gotos.take(tag)->append(reply);
    }
}

void SymbolTable::fillGotos()
{
    // Check all gotos have tags and all tags have gotos
    bool parseable = true;
    for (QString key : tags.keys()) {
        if (gotos.count(key) == 0) {
            qDebug() << "Tag without goto " << key;
            parseable = false;
        }
    }
    for (QString key : gotos.keys()) {
        if (tags.count(key) == 0) {
            qDebug() << "Goto without tag " << key;
            parseable = false;
        }
    }
    if (!parseable)
        return;

    for (QString key : tags.keys()) {
        Story* tag = tags.take(key);
        for (Story** jump : *gotos.take(key)) {
             *jump = tag;
        }
    }
}
