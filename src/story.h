#ifndef STORY_H
#define STORY_H

#include <QString>
#include <QList>
#include <QDebug>
#include "reply.h"
#include "symboltable.h"

class Story
{
public:
    QString name;

    enum NodeType {
        LoadMail,
        LoadCommonMail,
        LoadContact,
        LoadTemplate,
        LoadStory,
        DelContact,
        DelTemplate,
        WaitReply,
        WaitReplyCond,
        GoTo,
        Sleep,
        Web,
        Length // NOT A TYPE, only for loops
    };

    Story();
    ~Story();

    Story *next_node;
    Story *goto_node;
    Story *root_node;

    time_t until;
    bool until_valid;
    NodeType node_type;

    // Reply type
    QList<Reply*> replies;

    // Load.. type
    QStringList *list2load;

    static NodeType parse_type(QString type);
    static QString parse_type(Story::NodeType node_type);
    QString get_type();

    static Story *loadStory(QString filename);

    int length();
private:
    static Story *loadStoryLine(QString line, QString prefix);
};

#endif // STORY_H
