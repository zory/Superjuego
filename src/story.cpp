#include "mail.h"
#include "macros.h"
#include "story.h"

#include <QFile>

SymbolTable symbol_table; //??? Comprobar que funciona
QString tag;

Story::Story()
{
    this->list2load = new QStringList();
    this->until = 0;
    this->until_valid = false;
}

Story::~Story()
{
    delete this->list2load;
    for (Reply * rep : this->replies) {
        delete rep;
    }
    if (next_node != nullptr)
        delete next_node;
}

Story::NodeType Story::parse_type(QString type)
{
    for (int n = 0; n < NodeType::Length; n++) {
        Story::NodeType nt = (Story::NodeType)n;
        if(type.compare(parse_type(nt)) == 0) return nt;
    }
    qDebug() << "Tipo de nodo desconocido" << type;
    return NodeType::Sleep;
}

QString Story::parse_type(Story::NodeType node_type)
{
    if (node_type == NodeType::LoadMail) return "LoadMail";
    if (node_type == NodeType::LoadCommonMail) return "LoadCommonMail";
    if (node_type == NodeType::LoadContact) return "LoadContact";
    if (node_type == NodeType::LoadTemplate) return "LoadTemplate";
    if (node_type == NodeType::LoadStory) return "LoadStory";
    if (node_type == NodeType::DelContact) return "DelContact";
    if (node_type == NodeType::DelTemplate) return "DelTemplate";
    if (node_type == NodeType::WaitReply) return "WaitReply";
    if (node_type == NodeType::WaitReplyCond) return "WaitReplyCond";
    if (node_type == NodeType::GoTo) return "GoTo";
    if (node_type == NodeType::Sleep) return "Sleep";
    if (node_type == NodeType::Web) return "Web";

    return "Uknown node type";
}

QString Story::get_type()
{
    return parse_type(this->node_type);
}

Story* Story::loadStoryLine(QString line, QString prefix) {
    // Guardar tag y pasar al siguiente
    if (line.endsWith(":")) {
        tag = line.mid(0,line.length()-1);
        return nullptr;
    }
    // Comentario o linea vacia
    if (line.length() == 0 || line.startsWith("//"))
        return nullptr;

    Story *story = new Story();
    QStringList lineSplit = line.split(SEPARATOR1);

    story->name = prefix;
    story->node_type = Story::parse_type(lineSplit[0]);

    if (story->node_type == Story::LoadMail || story->node_type == Story::LoadCommonMail ||
            story->node_type == Story::LoadContact || story->node_type == Story::LoadTemplate ||
            story->node_type == Story::DelContact || story->node_type == Story::DelTemplate ||
            story->node_type == Story::LoadStory || story->node_type == Story::Sleep) {
        // Comprobar que solo tiene un elemento y se puede castear a int
        if (story->node_type == Story::Sleep) {
            assert(lineSplit.length() == 2);
            bool ok;
            lineSplit[1].toInt(&ok);
            assert(ok);
        }
        for (int i = 1; i < lineSplit.length(); i++) {
            QString element = lineSplit[i];
            // Comprobar sintaxis de LoadMail (contacto|mail) y añadir prefijo
            if (story->node_type == Story::LoadMail || (story->node_type == Story::LoadCommonMail && i == 1)) {
                QStringList split_mail = element.split(SEPARATOR2);
                assert(split_mail.length() == 2);
                element = split_mail[0] + SEPARATOR2 + prefix + "/" + split_mail[1];
            }
            // Añadir carpeta si es una plantilla
            if (story->node_type == Story::LoadTemplate || story->node_type == Story::DelTemplate)
                element = prefix + "/" + element;
            story->list2load->append(element);
        }
    } else if (story->node_type == Story::WaitReply) {
        for (int i = 1; i < lineSplit.length(); i++) {
            Reply* rep = new Reply();
            QStringList replySplit = lineSplit[i].split(SEPARATOR2);
            rep->recipient = replySplit[0];
            rep->template_name = prefix + "/" + replySplit[1];
            for (int j = 2; j < replySplit.length(); j++) {
                QString gap = replySplit[j];
                rep->answers->append(gap);
            }
            rep->answered = false;
            story->replies.append(rep);
        }
    } else if (story->node_type == Story::WaitReplyCond) {
        assert(lineSplit.length() % 2 == 1);
        for (int i = 1; i < lineSplit.length(); i+=2) {
            Reply* rep = new Reply();
            QStringList replySplit = lineSplit[i].split(SEPARATOR2);
            QString next_node_name = lineSplit[i+1];
            rep->recipient = replySplit[0];
            rep->template_name = prefix + "/" + replySplit[1];
            for(int j = 2; j < replySplit.length(); j++) {
                QString gap = replySplit[j];
                rep->answers->append(gap);
            }
            symbol_table.addGoto(next_node_name,&rep->next_node);
            story->replies.append(rep);
        }
    } else if (story->node_type == Story::GoTo) {
        QString next_node_name = lineSplit[1];
        symbol_table.addGoto(next_node_name,&story->goto_node);
    }
    // Añadir nodo a la lista de tags
    if (tag != nullptr) {
        symbol_table.addTag(tag,story);
        tag = nullptr;
    }
    qDebug() << "Parsed:" << story->get_type() << "Story:" << story->name;
    return story;
}

Story* Story::loadStory(QString filename)
{
    QString story_filename = ":/stories/" + filename + ".story";
    QFile file(story_filename);
    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "No se puede cargar la historia" << story_filename;
        return nullptr;
    }

    QTextStream *stream = new QTextStream(&file);
    stream->setCodec("UTF-8");

    // Parse root
    QString line = stream->readLine();
    Story* story;
    while ((story = loadStoryLine(line,filename)) == nullptr)
        line = stream->readLine();

    story->root_node = story;

    Story *aux_story = story;
    tag = nullptr;
    while (!stream->atEnd()) {
        line = stream->readLine();
        Story *aux2_story = loadStoryLine(line,filename);
        if (aux2_story == nullptr)
                continue;
        aux2_story->root_node = story;
        aux_story->next_node = aux2_story;
        aux_story = aux2_story;
    }
    if (tag != nullptr) {
        symbol_table.addTag(tag,nullptr);
        tag = nullptr;
    }
    aux_story->next_node = nullptr;
    file.close();
    symbol_table.fillGotos();

    return story;
}

int Story::length() {
    if (this->next_node == nullptr) {
        return 1;
    } else {
        return 1 + this->next_node->length();
    }
}
