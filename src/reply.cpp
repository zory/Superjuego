#include "reply.h"

#include <QStringList>

Reply::Reply()
{
    this->answers = new QStringList();
}

Reply::~Reply()
{
    delete this->answers;
}
