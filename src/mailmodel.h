#ifndef MAILMODEL_H
#define MAILMODEL_H

#include "mail.h"

#include <QModelIndex>
#include <QList>
#include <QAbstractListModel>
#include <QSortFilterProxyModel>

class MailModel:public QAbstractTableModel
{
    Q_OBJECT
public:
    MailModel(QObject * parent = 0);
    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    QVariant data(const QModelIndex &index, int role) const;
    QString getMailHtml(int row, int tray) const;
    QString getMailContact(const QModelIndex &index) const;
    void addMail(Mail *m);

    QList<Mail*> mDatas;
    QSortFilterProxyModel *filter;
};

#endif // MAILMODEL_H
