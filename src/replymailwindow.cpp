#include "replymailwindow.h"
#include "ui_replymailwindow.h"
#include "templateview.h"
#include "mainwindow.h"

#include <QDebug>
#include <QDesktopWidget>

ReplyMailWindow::ReplyMailWindow(QWidget *parent, QString replyField) :
    QDialog(parent),
    ui(new Ui::ReplyMailWindow)
{
    ui->setupUi(this);

    tv = new TemplateView();
    ui->mailTextArea->addWidget(tv);

    if (replyField==nullptr)
        ui->contactComboBox->addItems(*((MainWindow*)this->parent())->contactModel);
    else
        ui->contactComboBox->addItem(replyField);

    model = ((MainWindow*)((ReplyMailWindow*)this->parent()))->templateModel;

    ui->listView->setModel(model);

//    Template *t = model->getTemplate(0);
//    this->setTemplate(t);

    ui->splitter->setSizes({1, 1000});
    resize(QDesktopWidget().availableGeometry(this).size() * 0.5);
}

ReplyMailWindow::~ReplyMailWindow()
{
    delete tv;
    delete ui;
}

void ReplyMailWindow::setTemplate(Template *t)
{
    this->t = t;
    ui->subject->setText(t->subject);
    tv->buildList(t->text);
    tv->paintWindow();
}

void ReplyMailWindow::on_replyButton_clicked()
{
    if (t == nullptr) {
        qDebug() << "Implementar si no hay template seleccionado";
        return;
    }
    QString recipient = ui->contactComboBox->currentText();
    QStringList *answers = tv->getGaps();
    ((MainWindow*)parent())->recieveReply(recipient,t,answers);
    this->close();
}

void ReplyMailWindow::on_listView_clicked(const QModelIndex &index)
{
    Template *t = model->getTemplate(index.row());
    this->setTemplate(t);
}
