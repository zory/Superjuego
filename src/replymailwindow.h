#ifndef REPLYWINDOW_H
#define REPLYWINDOW_H

#include "template.h"
#include "templatemodel.h"
#include "templateview.h"

#include <QDialog>

QT_BEGIN_NAMESPACE
namespace Ui {class ReplyMailWindow;}
QT_END_NAMESPACE

class ReplyMailWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ReplyMailWindow(QWidget *parent = nullptr, QString replyField = nullptr);
    ~ReplyMailWindow();

    void setTemplate(Template *t);

private slots:
    void on_replyButton_clicked();    
    void on_listView_clicked(const QModelIndex &index);

private:
    Template *t;
    TemplateView *tv;
    TemplateModel *model;
    Ui::ReplyMailWindow *ui;
};

#endif // REPLYWINDOW_H
