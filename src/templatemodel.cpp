#include "templatemodel.h"

TemplateModel::TemplateModel(QObject *parent):QAbstractListModel(parent)
{

}

int TemplateModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return mDatas.count();
}

QVariant TemplateModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::DisplayRole)
    {
        if (index.column() == 0)
            return mDatas[index.row()]->subject;

        if (index.column() == 1)
            return mDatas[index.row()]->text;
    }

    return QVariant();
}

void TemplateModel::addTemplate(Template *t)
{
    beginInsertRows(QModelIndex(),mDatas.size(),mDatas.size()+1);
    mDatas.append(t);
    endInsertRows();
}

void TemplateModel::removeTemplate(const QString &t)
{
    for (int i = 0; i < mDatas.length(); i++) {
        if (mDatas[i]->name.compare(t) == 0) {
            beginRemoveRows(QModelIndex(),i,i);
            mDatas.removeAt(i);
            endRemoveRows();
        }
    }
}

Template* TemplateModel::getTemplate(const int index)
{
    return mDatas[index];
}
