#ifndef TEMPLATEVIEW_H
#define TEMPLATEVIEW_H

#include <QGraphicsView>

class TemplateView : public QGraphicsView
{
    Q_OBJECT
public:
    TemplateView(QWidget *parent = nullptr);
    ~TemplateView();

    void buildList(const QString sentence);
    void paintWindow();
    QStringList* getGaps();

private:
    QList<QWidget*> wordsList;
    QList<QWidget*> gapsList;
    QGraphicsScene *scene;

    void resizeEvent(QResizeEvent *event);

};

#endif // TEMPLATEVIEW_H
