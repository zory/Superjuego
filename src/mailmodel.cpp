#include "mailmodel.h"

#include <QFont>

MailModel::MailModel(QObject *parent):QAbstractTableModel(parent)
{
    filter = new QSortFilterProxyModel();
    filter->setDynamicSortFilter(true);
    filter->setSourceModel(this);
    filter->setSortRole(Qt::EditRole);
    filter->sort(2,Qt::DescendingOrder);
}

int MailModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return mDatas.count();
}

int MailModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 3;
}

QVariant MailModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
    {
        switch (section)
        {
        case 0:
            return tr("Asunto");
        case 1:
            return tr("Remitente");
        case 2:
            return tr("Fecha");
        default:
            return QString("No implementado");
        }

    }

    return QVariant();
}

QVariant MailModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::DisplayRole)
    {
        if (index.column() == 0)
            return mDatas[index.row()]->subject;

        if (index.column() == 1)
            return mDatas[index.row()]->from;

        if (index.column() == 2) {
            time_t timestamp = mDatas[index.row()]->timestamp;
            std::tm * ptm = std::localtime(&timestamp);
            char buffer[32];
            // Format: Mo, 15.06.2009 20:20:00
            std::strftime(buffer, 32, "%a, %d.%m.%Y %H:%M:%S", ptm);
            return buffer;
        }
    }

    if (role == Qt::EditRole)
    {
        if (index.column() == 0)
            return mDatas[index.row()]->subject;

        if (index.column() == 1)
            return mDatas[index.row()]->from;

        if (index.column() == 2) {
            return mDatas[index.row()]->timestamp;
        }
    }

    if (role == Qt::FontRole)
    {
        QFont font;
        font.setBold(mDatas[index.row()]->unread);
        return font;
    }

    return QVariant();
}

QString MailModel::getMailHtml(int row, int tray) const
{
    QString html;
    if (tray == 0) {
        html = "<b>From:</b> " + mDatas[row]->from + "<br/>";
    } else if (tray == 1) {
        html = "<b>To:</b> " + mDatas[row]->from + "<br/>";
    }
    html += "<b>Subject:</b> " + mDatas[row]->subject + "<br/>";
    html += "<br/>";
    html += mDatas[row]->text;
    html += "<br/>";
    return html;
}

QString MailModel::getMailContact(const QModelIndex &index) const
{
    int _index = filter->mapToSource(index).row();
    return mDatas[_index]->from;
}

void MailModel::addMail(Mail *m)
{
    beginInsertRows(QModelIndex(),mDatas.size(),mDatas.size());
    mDatas.append(m);
    endInsertRows();
}
