#ifndef MAIL_H
#define MAIL_H

#include <QString>

class Mail
{
public:
    Mail();
    Mail(const QString& filename, const QString& from, const QString& subject, const QString& text, bool unread, time_t timestamp);

    QString name;
    QString from;
    QString subject;
    QString text;
    time_t timestamp;
    bool unread;
    QStringList *answers; // For exit tray
    static Mail *loadMail(QString filename, QString from, bool unread, time_t timestamp);
};

#endif // MAIL_H
