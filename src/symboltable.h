#ifndef SYMBOLTABLE_H
#define SYMBOLTABLE_H

#include <QHash>
#include <QList>
#include <QString>
#include "reply.h"

class Story;

class SymbolTable
{
public:
    SymbolTable();

    QHash<QString,Story*> tags;
    QHash<QString,QList<Story**>*> gotos;

    void addTag(QString tag, Story* story);
    void addGoto(QString tag, Story** reply);
    void fillGotos();
};

#endif // SYMBOLTABLE_H
