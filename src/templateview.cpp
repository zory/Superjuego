#include "templateview.h"
#include "macros.h"

#include <QLabel>
#include <QLineEdit>
#include <QDebug>
#include <QTextEdit>
#include <QComboBox>

TemplateView::TemplateView(QWidget *parent) :
    QGraphicsView(parent)
{
    wordsList = QList<QWidget*>();
    gapsList = QList<QWidget*>();
    scene = new QGraphicsScene(this);
    this->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    this->setScene(scene);
}

void TemplateView::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event)
    paintWindow();
}

void TemplateView::buildList(const QString sentence)
{
    scene->clear();
    wordsList.clear();

    QStringList words_aux = sentence.split("\n");
    QStringList words;
    for(int i = 0; i < words_aux.size(); i++) {
        QString aux = words_aux[i];
        if (aux.compare("<ul>") == 0) {
            QString list = "";
            while (i < words_aux.size() && words_aux[i].compare("</ul>") != 0) {
                aux = words_aux[i];
                list += aux + "\n";
                i++;
            }
            words.append(list);
            words.append("\n");
        } else {
            words.append(aux.split(QRegExp("\\s+")));
            words.append("\n");
        }
    }

    for(QString word : words) {
        QWidget *proxy = new QWidget(this);
        QSizePolicy pol = QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
        if (word.startsWith("{") && word.endsWith("}")) {
            bool isLineEdit;
            QString content = word.mid(1,word.length()-2);
            int width = content.toInt(&isLineEdit);
            if (isLineEdit) {
                QLineEdit *edit = new QLineEdit();
                edit->setFixedHeight(18);
                edit->setFixedWidth(width*18);
                gapsList.append(edit);
                proxy = edit;
            } else {
                QStringList options = content.split(SEPARATOR_GAPS_COMBOBOX);
                QComboBox *box = new QComboBox();
                box->setFixedHeight(18);
                box->addItems(options);
                gapsList.append(box);
                proxy = box;
            }
        } else if (word.startsWith("<ul>")) {
            QLabel* label = new QLabel(word);
            label->setAttribute(Qt::WA_TranslucentBackground);
            proxy = label;
        } else {
            QLabel *label = new QLabel(word);
            label->setAttribute(Qt::WA_TranslucentBackground);
            label->setFixedHeight(18);
//            label->setFrameStyle(QFrame::Box | QFrame::Plain);
            proxy = label;
        }

        proxy->setSizePolicy(pol);
        proxy->adjustSize();
        wordsList.append(proxy);

        scene->addWidget(proxy);
    }
}

void TemplateView::paintWindow()
{

    int left, top, right;
    QMargins margins= this->contentsMargins();
    left = margins.left(); right = margins.right(); top = margins.top();
    const qreal maxw = this->width() - left - right;

    qreal x = 0;
    qreal y = 0;
    qreal maxRowHeight = 0;

    for (QWidget *proxy : wordsList) {
        QLabel* word_aux = dynamic_cast<QLabel*>(proxy);
        bool new_line = word_aux != nullptr && word_aux->text().compare("\n") == 0;

        if (new_line) {
            proxy->hide();
            x = 0;
            QSizeF pref = proxy->size();
            maxRowHeight = qMax(maxRowHeight, pref.height());
            y += maxRowHeight;
            maxRowHeight = 0;
        } else {
            QSizeF pref = proxy->size();
            maxRowHeight = qMax(maxRowHeight, pref.height());

            qreal next_x;
            next_x = x + pref.width();
            if (x != 0 && next_x > maxw-18) {
                if (qFuzzyIsNull(x)) {
                    pref.setWidth(maxw);
                } else {
                    x = 0;
                    next_x = pref.width();
                }
                y += maxRowHeight;
                maxRowHeight = 0;
            }

            proxy->setGeometry(left+x,top+y,pref.width(),pref.height());/*QRectF(QPointF(left + x, top + y), pref));*/

            x = next_x + 5;
        }
    }
    QSize gSize = this->size();
    qreal h;
    qreal w;
    if (gSize.height()-9 > y) {
        h = gSize.height() - 9;
        w = gSize.width() - 9;
    } else {
        h = y + maxRowHeight;
        w = gSize.width() - 22;
    }
    scene->setSceneRect(0,0,w,h); // TODO fix hardcoded values and do it properly
}

QStringList *TemplateView::getGaps()
{
    QStringList *list = new QStringList();
    for(QWidget *aux : gapsList) {
        QLineEdit *edit = dynamic_cast<QLineEdit*>(aux);
        if (edit) {
            list->append(edit->text());
            continue;
        }
        QComboBox *box = dynamic_cast<QComboBox*>(aux);
        if (box) {
            list->append(box->currentText());
            continue;
        }
        qDebug() << "Incorrect type of gap";
    }
    return list;
}

TemplateView::~TemplateView()
{
    scene->clear();
    delete scene;
    wordsList.clear();
    gapsList.clear();
}
