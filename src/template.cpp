#include "template.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>

// Ejemplo
Template::Template()
{
}

Template::Template(const QString name, const QString subject, const QString text)
{
    this->name = name;
    this->subject = subject;
    this->text = text;
}

Template* Template::loadTemplate(QString filename)
{
    QString filename_template = ":/templates/" + filename + ".template";
    QFile file(filename_template);
    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "No se puede cargar el template" << filename;
        return nullptr;
    }
    QTextStream *stream = new QTextStream(&file);
    stream->setCodec("UTF-8");
    QString templateSubject = stream->readLine();
    QString tempalteText = stream->readAll();
    Template *t = new Template(filename,templateSubject,tempalteText);
    file.close();
    return t;
}
