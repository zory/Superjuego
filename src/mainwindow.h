#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "mailmodel.h"
#include "story.h"
#include "templatemodel.h"
#include "macros.h"

#include <QMainWindow>
#include <QMediaPlayer>
#include <QSettings>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void recieveReply(QString recipient, Template *t, QStringList *answers);

    MailModel *mailModel;
    MailModel *listTrayModel[TRAY_SIZE];
    QStringList *contactModel;
    TemplateModel *templateModel;
    QList<Story*> current_stories;
    long score;
    QSettings state;

    QMediaPlayer *sounds[SOUND_SIZE];


    void closeEvent(QCloseEvent *event);
    void saveGame();
    bool loadGame();

public slots:
    void continueStory(int i);

private slots:
    void on_mailList_clicked(const QModelIndex &index);

    void on_compose_clicked();
    void on_reply_clicked();

    void on_trayList_clicked(const QModelIndex &index);

private:
    void addMailExitTray(QString recipient, Template *t, QStringList *answers, time_t timestamp = 0);
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
