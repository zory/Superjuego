#ifndef TEMPLATEMODEL_H
#define TEMPLATEMODEL_H

#include "template.h"

#include <QAbstractListModel>
#include <QString>
#include <QVariant>

class TemplateModel:public QAbstractListModel
{
    Q_OBJECT
public:
    TemplateModel(QObject * parent = 0);
    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    void addTemplate(Template *t);
    void removeTemplate(const QString &t);
    Template *getTemplate(const int index);

    QList<Template*> mDatas;
};

#endif // TEMPLATEMODEL_H
